import { useState } from 'react';
import VCard from './VCard';

const csvStringToArray = (strData) => {
  const objPattern = new RegExp(
    '(,|\\r?\\n|\\r|^)(?:"([^"]*(?:""[^"]*)*)"|([^,\\r\\n]*))',
    'gi'
  );
  let arrMatches = null,
    arrData = [[]];
  while ((arrMatches = objPattern.exec(strData))) {
    if (arrMatches[1].length && arrMatches[1] !== ',') arrData.push([]);
    arrData[arrData.length - 1].push(
      arrMatches[2]
        ? arrMatches[2].replace(new RegExp('""', 'g'), '"')
        : arrMatches[3]
    );
  }
  return arrData;
};

const arrayToObj = ([header, ...rest]) =>
  rest
    .filter((x) => x.length === header.length)
    .map((xs) =>
      xs.reduce(
        (obj, x, index) => ({
          ...obj,
          [header[index]]: x,
        }),
        {}
      )
    );

const csvToObjectArray = (data) => arrayToObj(csvStringToArray(data));

function App() {
  const [people, setPeople] = useState([]);
  return (
    <div>
      <div className="description-container">
        <p>
          Bitte CSV-Datei mit drei Spalten <em>firstName</em>, <em>lastName</em>{' '}
          und <em>workPhone</em> mit Zeichensatz UTF-8 auswählen.
        </p>
        <input
          type="file"
          accept="text/csv"
          onChange={(e) => {
            if (e.target.files.length > 0) {
              e.target.files
                .item(0)
                .text()
                .then((x) => setPeople(csvToObjectArray(x)));
            }
          }}
        />
      </div>
      <div>
        {people.map((p) => (
          <VCard key={JSON.stringify(p)} {...p} />
        ))}
      </div>
    </div>
  );
}

export default App;
