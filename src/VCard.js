import QRCode from 'easyqrcodejs';
import vCardsJS from 'vcards-js';
import { useEffect, useRef } from 'react';

function VCard({ firstName, lastName, workPhone }) {
  const code = useRef(null);
  useEffect(() => {
    const vCard = vCardsJS();

    vCard.firstName = firstName;
    vCard.lastName = lastName;
    vCard.workPhone = workPhone;

    new QRCode(code.current, {
      text: vCard.getFormattedString(),
      logo: './image.terdiv4_badge.jpg',
      width: 128,
      height: 128,
    });
  }, [code, firstName, lastName, workPhone]);
  return (
    <div className="vcard">
      <strong>{firstName} {lastName}</strong>
      <div ref={code} />
    </div>
  );
}

export default VCard;
